package com.ninestack.nestedtab.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ninestack.nestedtab.MainActivity;
import com.ninestack.nestedtab.di.BroadcastScoped;
import com.ninestack.nestedtab.fragments.firsttab.FirstFragment;

import javax.inject.Inject;

import dagger.android.AndroidInjection;


/**
 * Created by Adrian Almeida.
 */

public class DummyBroadcastReceiver extends BroadcastReceiver {

    @Inject
    Notifier notifier;

    @Override
    public void onReceive(Context context, Intent intent) {
        AndroidInjection.inject(this, context);

        final String action = intent.getAction();
        if (action.equals(MainActivity.ACTION_SEND_NOTIFICATION)) {
            notifier.notify(intent.getStringExtra(MainActivity.EXTRA_DATA));
        }

        if (action.equals(FirstFragment.EXTRA_DATA_FRAGMENT)){
            notifier.notify(intent.getStringExtra(FirstFragment.EXTRA_DATA_FRAGMENT));
        }
    }
}
