package com.ninestack.nestedtab.broadcast;

import android.app.Application;
import android.content.Context;

import com.ninestack.nestedtab.App;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Adrian Almeida.
 */
@Module
public class NotiferModule {
    @Provides
    @Singleton
    Notifier notifier(App app) {
        return new Notifier(app.getApplicationContext());
    }


}
