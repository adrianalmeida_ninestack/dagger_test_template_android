package com.ninestack.nestedtab.broadcast;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Adrian Almeida.
 */
@Module
public abstract class BroadcastModule {

    @ContributesAndroidInjector
    abstract DummyBroadcastReceiver dummyBroadcastReceiver();

/*    @Singleton
    @Provides
    Notifier provideSharedPrefsHelper(Application context) {
        return new Notifier(context.getApplicationContext());
    }*/


 /*   @Singleton
    @Provides
    Notifier provideRxBus(Context context) {
        return new Notifier(context);
    }*/
}
