package com.ninestack.nestedtab.broadcast;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

import static android.content.ContentValues.TAG;

/**
 * Created by Adrian Almeida.
 */
//https://stackoverflow.com/a/47058362 for injecting broadcast
public class Notifier {
    private Context context;

    @Inject
    public Notifier(Context context) {
        this.context = context;
    }

    public void notify(String message) {
        Log.i(TAG, "notify: testvyhcassc");
        if (context != null)
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
