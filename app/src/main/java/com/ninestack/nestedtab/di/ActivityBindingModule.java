package com.ninestack.nestedtab.di;

import com.ninestack.nestedtab.MainActivity;
import com.ninestack.nestedtab.fragments.firsttab.FirstTabModule;
import com.ninestack.nestedtab.fragments.secondtab.SecondFragment;
import com.ninestack.nestedtab.fragments.secondtab.SecondTabModule;
import com.ninestack.nestedtab.fragments.thirdtab.ThirdTabModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Adrian Almeida.
 */
// @ContributesAndroidInjector was introduced removing the need to define @Subcomponent classes.
@Module
public abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = {FirstTabModule.class, SecondTabModule.class, ThirdTabModule.class})
    abstract MainActivity dashboardActivity();
}
