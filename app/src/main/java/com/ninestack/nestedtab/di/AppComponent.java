package com.ninestack.nestedtab.di;

import com.ninestack.nestedtab.App;
import com.ninestack.nestedtab.broadcast.BroadcastModule;
import com.ninestack.nestedtab.broadcast.NotiferModule;
import com.ninestack.nestedtab.service.ServiceModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.Module;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by Adrian Almeida.
 */
@Singleton
@Component(modules = {AndroidSupportInjectionModule.class,
        ServiceModule.class,ApplicationModule.class
        ,ActivityBindingModule.class, BroadcastModule.class, NotiferModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(App application);

        AppComponent build();
    }

    void inject(App app);
}
