package com.ninestack.nestedtab.di;

import javax.inject.Scope;

/**
 * Created by Adrian Almeida.
 */
@Scope
public @interface ActivityScoped {
}
