package com.ninestack.nestedtab.fragments.firsttab;

import com.ninestack.nestedtab.di.FragmentScoped;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Adrian Almeida.
 */
@Module
public abstract class FirstTabModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract FirstFragment firstFragment();
}
