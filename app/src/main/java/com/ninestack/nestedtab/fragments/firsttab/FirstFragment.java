package com.ninestack.nestedtab.fragments.firsttab;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ninestack.nestedtab.R;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends DaggerFragment {

    public static final String ACTION_FRAGMENT = "com.ninestack.nestedtab.broadcast";
    public static final String EXTRA_DATA_FRAGMENT = "data";


    @Inject
    public FirstFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_first, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @OnClick(R.id.btn)
    public void OnClick(){
        broadcastMessage("Click working from fragment");
    }
    private void broadcastMessage(final String message) {
        Intent intent = new Intent();
        intent.setAction(ACTION_FRAGMENT)
                .putExtra(EXTRA_DATA_FRAGMENT, message);
        getActivity().sendBroadcast(intent);
    }




}
