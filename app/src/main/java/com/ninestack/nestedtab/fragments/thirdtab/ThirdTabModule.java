package com.ninestack.nestedtab.fragments.thirdtab;

import com.ninestack.nestedtab.di.FragmentScoped;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Adrian Almeida.
 */
@Module
public abstract class ThirdTabModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract ThirdFragment thirdFragment();
}
