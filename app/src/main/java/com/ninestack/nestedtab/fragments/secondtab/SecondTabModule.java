package com.ninestack.nestedtab.fragments.secondtab;

import com.ninestack.nestedtab.di.FragmentScoped;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Adrian Almeida.
 */
@Module
public abstract class SecondTabModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract SecondFragment secondFragment();

}
