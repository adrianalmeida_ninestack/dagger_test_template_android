package com.ninestack.nestedtab.service;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Adrian Almeida.
 */
@Module
public abstract class ServiceModule {

    @ContributesAndroidInjector
    abstract SomeService downloadService();
}
