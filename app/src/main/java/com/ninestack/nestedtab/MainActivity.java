package com.ninestack.nestedtab;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.ninestack.nestedtab.fragments.firsttab.FirstFragment;
import com.ninestack.nestedtab.fragments.secondtab.SecondFragment;
import com.ninestack.nestedtab.fragments.thirdtab.ThirdFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

public class MainActivity extends DaggerAppCompatActivity {

    FragmentTransaction fragmentTransaction;

    FragmentManager mFragmentManager;

    @Inject
    FirstFragment firstFragment;
    @Inject
    SecondFragment secondFragment;
    @Inject
    ThirdFragment thirdFragment;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    public static final String ACTION_SEND_NOTIFICATION = "com.ninestack.nestedtab.broadcast";
    public static final String EXTRA_DATA = "data";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setuptabLayout();//tab layout initialization
        setUpFirstTab();//setting up first tab
    }

    private void setUpFirstTab() {
        mFragmentManager = getSupportFragmentManager();
        Fragment mFragment = mFragmentManager.findFragmentById(R.id.fragment_container);

        if (mFragment == null) {
            mFragment = firstFragment;
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.fragment_container, mFragment);
            fragmentTransaction.commit();
        }
    }

    private void setuptabLayout() {
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.menu));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.menu));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.menu));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tabLayout.getSelectedTabPosition()) {
                    case 0:
                        broadcastMessage("Broadcast working!!");
                        setFragment(firstFragment);
                        break;
                    case 1:
                        setFragment(secondFragment);
                        break;
                    case 2:
                        setFragment(thirdFragment);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }
    
    private void setFragment(Fragment mSelectedFragment) {
        fragmentTransaction = mFragmentManager.beginTransaction();

        if (!(mSelectedFragment instanceof FirstFragment)) {
            if (mFragmentManager.getBackStackEntryCount() == 0) {
                if (!mSelectedFragment.isAdded())
                    fragmentTransaction.add(R.id.fragment_container, mSelectedFragment).addToBackStack(mSelectedFragment.getTag());
            } else {
                fragmentTransaction.replace(R.id.fragment_container, mSelectedFragment);
            }
        } else {
            mFragmentManager.popBackStack();
            fragmentTransaction.replace(R.id.fragment_container, mSelectedFragment);
        }
        fragmentTransaction.commit();
    }


    private void broadcastMessage(final String message) {
        Intent intent = new Intent();
        intent.setAction(ACTION_SEND_NOTIFICATION)
                .putExtra(EXTRA_DATA, message);
        sendBroadcast(intent);
    }

}
